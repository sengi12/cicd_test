#!/usr/bin/env python
import unittest
from project.factor import factorial

class TestFactorial(unittest.TestCase):

    def test_fact(self):
        '''
        Trivial Test Case
        '''
        self.assertEqual(factorial(5), 120)

    def test_errors(self):
        '''
        Handle invalid parameters as expected
        '''
        with self.assertRaises(TypeError):
            factorial("string_example")
