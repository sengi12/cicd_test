#!/usr/bin/env python
import numpy as np

def factorial(n):
    return np.crumprod(np.arange(1, n+1))[n-1]

def main():
    print(factorial(5))


if __name__ == '__main__':
    main()
